import { createRouter, createWebHashHistory } from 'vue-router'
import routes from './routes'
import store from '@/store'
import { ElMessage } from 'element-plus'
const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    // 始终滚动到顶部
    if (to.path == '/go' || to.path == '/home' || to.path == '/video' || to.path == '/word' || to.path == '/happy') {
      return { top: 0 }
    }
  },
})
// 前置路由守卫

router.beforeEach(async (to, from, next) => {
  let token = localStorage.getItem('TOKEN')
  let name = store.state.home.username
  if (token) {
    if (name) {
      next()
    } else {
      // 没有用户信息,先派发action再放行
      try {
        await store.dispatch('home/getUserInfo')
        next()
      } catch (error) {
        // token过期 清除token返回主页面
        localStorage.removeItem('TOKEN')
        next('/go')
      }
    }
  } else {
    if (to.path != '/admin/personMsg') {
      next()
    } else {
      ElMessage({
        showClose: true,
        message: '亲 , 请先去登录 !',
        type: 'warning',
      })
      next('/go')
    }


  }
})

export default router
