export default [
    // 后台管理
    {
        path: '/manage',
        name: 'manage',
        component: () => import('@/views/Manage'),
        redirect: "/manage/mhome",
        children: [
            {
                path: '/manage/articlelist',
                name: 'articlelist',
                component: () => import('@/views/ArticleList')
            },
            {
                path: '/manage/leibie',
                name: 'leibie',
                component: () => import('@/views/LeiBie')
            },
            {
                path: '/manage/pubarticle',
                name: 'pubarticle',
                component: () => import('@/views/PubArticle')
            },
            {
                path: '/manage/mhome',
                name: 'mhome',
                component: () => import('@/views/Mhome')
            },
            {
                path: '/manage/managemsg',
                name: 'managemsg',
                component: () => import('@/views/ManageMsg')
            },
            {
                path: '/manage/manageavatar',
                name: 'manageavatar',
                component: () => import('@/views/ManageAvatar')
            },
            {
                path: '/manage/managepwd',
                name: 'managepwd',
                component: () => import('@/views/ManagePwd')
            },

        ]
    },
    {
        path: '/article',
        name: 'article',
        component: () => import('@/views/Article'),
        meta: {
            show: false
        }
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('@/views/Home'),
        meta: {
            show: false
        }
    },
    {
        path: '/person',
        name: 'person',
        component: () => import('@/views/Person'),
        meta: {
            show: false
        }
    },
    {
        path: '/search',
        name: 'search',
        component: () => import('@/views/Search'),
        meta: {
            show: false
        }
    },
    {
        path: '/video',
        name: 'video',
        component: () => import('@/views/Video'),
        meta: {
            show: true
        }
    },
    {
        path: '/go',
        name: 'go',
        component: () => import('@/views/Go'),
        meta: {
            show: true
        }
    },
    {
        path: '/happy',
        name: 'happy',
        component: () => import('@/views/Happy'),
        meta: {
            show: false
        }
    },
    //   个人中心
    {
        path: '/admin',
        name: 'admin',
        redirect: "/admin/personMsg",
        component: () => import('@/views/Admin'),
        meta: {
            show: false
        },
        children: [
            {
                path: '/admin/haveDo',
                name: 'haveDo',
                component: () => import('@/views/HaveDo')
            },
            {
                path: '/admin/mood',
                name: 'mood',
                component: () => import('@/views/Mood')
            },
            {
                path: '/admin/personMsg',
                name: 'PersonMsg',
                component: () => import('@/views/PersonMsg')
            },
            {
                path: '/admin/changePwd',
                name: 'changePwd',
                component: () => import('@/views/ChangePwd')
            },
            {
                path: '/admin/userInfo',
                name: 'userInfo',
                component: () => import('@/views/UserInfo')
            },


        ]
    },
    {
        path: '/word',
        name: 'word',
        component: () => import('@/views/Word'),
        meta: {
            show: false
        }
    },
    {
        path: '/',
        redirect: '/home'
    }

]