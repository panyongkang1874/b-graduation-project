
import { reqArticlePub, reqArticles, reqDeleteArticle } from '@/api'
const actions = {
    async getMsg(contest) {
        let result = await reqArticles()
        console.log(result);
        if (result.status == 0) {
            contest.commit('GETARTICLES', result.data)
        }
    },
    // 新增留言
    async getArticlesPub(contest, data) {
        let result = await reqArticlePub(data)
        if (result.status == 0) {
            return 'ok'
        } else {
            Promise.reject(new Error('fail'))
        }
    },
    // 删除留言
    async deleteArticle(contest, id) {
        console.log(id);
        let result = await reqDeleteArticle(id)
        if (result.status == 0) {
            return 'ok'
        } else {
            return Promise.reject(new Error('fail'))
        }
    }
}
const mutations = {
    // 新增留言
    GETARTICLES(state, value) {
        state.articles = value
    }
}
const state = {
    articles: []
}
const getters = {}
export default {
    namespaced: true,
    actions,
    mutations,
    state,
    getters
}