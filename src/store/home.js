import { reqCarousel, reqAlbum, reqMissing, reqUserInfo } from '@/api/index'


const actions = {
    // 轮播图
    async getCarousel(contest) {
        let result = await reqCarousel()
        if (result.code == 200) {
            contest.commit('GETCAROUSEL', result.data)
        }
    },
    // 卡片
    async getAlbum(contest) {
        let result2 = await reqAlbum()
        if (result2.code == 200) {
            contest.commit('GETALBUM', result2.data)
        }
    },
    // miss模块
    async getMissing(contest) {
        let result3 = await reqMissing()
        if (result3.code == 200) {
            contest.commit('GETMISSING', result3.data)
        }
    },
    // 获取用户信息
    async getUserInfo(contest) {
        let result4 = await reqUserInfo()
        // console.log(result4);
        if (result4.status == 0) {
            contest.commit('GETUSERINFO', result4.data)
        }
    }

}
const mutations = {
    // 轮播图
    GETCAROUSEL(state, value) {
        state.pics = value
    },
    // 卡片
    GETALBUM(state, value) {
        state.album = value
    },
    // miss模块
    GETMISSING(state, value) {
        state.missing = value
    },
    // 获取用户信息
    GETUSERINFO(state, value) {
        state.userInfo = value
    }
}
const state = {
    pics: [],
    album: [],
    missing: [],
    userInfo: {}
}
const getters = {}
export default {
    namespaced: true,
    actions,
    mutations,
    state,
    getters
}