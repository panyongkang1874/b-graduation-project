import { reqLiuyan, reqAddLiuyan, reqDeleteLiuyan } from "@/api"


const actions = {
    // 获取留言
    async getLiuyan(contest) {
        let result = await reqLiuyan()
        console.log(result);
        if (result.status == 0) {
            contest.commit('GETLIUYAN', result.data)
        }
    },
    // 新增留言
    async addLiuyan(contest, data) {
        let result2 = await reqAddLiuyan(data)
        console.log(result2);
    },
    // 删除留言
    async deleteLiuyan(contest, id) {
        let result = await reqDeleteLiuyan(id)
        // console.log(result);
        if (result.status == 0) {
            return 'ok'
        } else {
            return Promise.reject(new Error('fail'))
        }
    }
}
const mutations = {
    // 获取留言
    GETLIUYAN(state, value) {
        state.liuyan = value
    }
}
const state = {
    liuyan: []
}
const getters = {}
export default {
    namespaced: true,
    actions,
    mutations,
    state,
    getters
}