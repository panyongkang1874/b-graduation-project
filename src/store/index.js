import { createStore } from 'vuex'
import home from '@/store/home'
import admin from '@/store/admin'
import video from '@/store/video'
import search from '@/store/search'
import person from '@/store/person'
import manage from '@/store/manage'
import article from './article'
export default createStore({
  modules: {
    home,
    admin,
    video,
    search,
    person,
    manage,
    article
  }
})
