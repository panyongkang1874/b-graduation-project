import { reqUserInfoById } from '@/api'
const actions = {
    async getPerson(contest, data) {
        let result = await reqUserInfoById(data)
        if (result.status == 0) {
            contest.commit('GETPERSON', result.data)
        }
    }
}
const mutations = {
    GETPERSON(state, value) {
        state.personMsg = value
    }
}
const state = {
    personMsg: {}
}
const getters = {}
export default {
    namespaced: true,
    actions,
    mutations,
    state,
    getters
}