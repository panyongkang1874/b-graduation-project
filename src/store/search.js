import { reqSearchInfo } from '@/api'
const actions = {
    async getSearch(contest, data) {
        let result = await reqSearchInfo(data)
        if (result.status == 0) {
            contest.commit('GETSEARCH', result.data)
        }
    }
}
const mutations = {
    GETSEARCH(state, value) {
        state.searchInfo = value
    }
}
const state = {
    searchInfo: []
}
const getters = {}
export default {
    namespaced: true,
    actions,
    mutations,
    state,
    getters
}