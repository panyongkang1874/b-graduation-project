import { reqUserPwd, reqAvatar, reqUpdateUserInfo } from "@/api"
// 改密码
const actions = {
    async changePwd(contest, data) {
        let result = await reqUserPwd(data)
        // console.log(result);
        if (result.status === 0) {
            return 'ok'
        } else {
            return Promise.reject(new Error('fail'))
        }
    },
    // 更新用户信息
    async changeUserInfo(contest, data) {
        let result = await reqUpdateUserInfo(data)
        console.log(result);
        if (result.status == 0) {
            return 'ok'
        } else {
            return Promise.reject(new Error('fail'))
        }
    },
    // 改头像
    async changePic(contest, data) {
        let result = await reqAvatar(data)
        console.log(result);
    }

}


const mutations = {
    CHANGEPWD(state, value) {
        state.message = value
    }
}
const state = {
    message: ''
}
const getters = {}
export default {
    namespaced: true,
    actions,
    mutations,
    state,
    getters
}