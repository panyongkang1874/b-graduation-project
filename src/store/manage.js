import { reqUserPwd, reqAvatar, reqUpdateUserInfo, reqAllUserInfo, reqDeleteUser } from "@/api"

const actions = {
    // 改密码
    async changePwd(contest, data) {
        let result = await reqUserPwd(data)
        // console.log(result);
        if (result.status === 0) {
            return 'ok'
        } else {
            return Promise.reject(new Error('fail'))
        }
    },
    // 更新用户信息
    async changeUserInfo(contest, data) {
        let result = await reqUpdateUserInfo(data)
        console.log(result);
        if (result.status == 0) {
            return 'ok'
        } else {
            return Promise.reject(new Error('fail'))
        }
    },
    // 改头像
    async changePic(contest, data) {
        let result = await reqAvatar(data)
        console.log(result);
    },
    // 获取所有用户信息
    async getAllUserInfo(contest) {
        let result = await reqAllUserInfo()
        if (result.status == 0) {
            contest.commit('GETALLUSERINFO', result.data)
        }
    },
    // 删除用户
    async delete(contest, data) {
        let result = await reqDeleteUser(data)
        if (result.status == 0) {
            return 'ok'
        } else {
            return Promise.reject(new Error('fail'))
        }
    }

}


const mutations = {
    CHANGEPWD(state, value) {
        state.message = value
    },
    // 获取所有用户信息
    GETALLUSERINFO(state, value) {
        state.allUserInfo = value
    }
}
const state = {
    message: '',
    allUserInfo: []
}
const getters = {}
export default {
    namespaced: true,
    actions,
    mutations,
    state,
    getters
}