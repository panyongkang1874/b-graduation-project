import request from "@/api/request";
import userInfo from '@/api/userInfo'
import mockRequest from '@/api/mockRequest'
import liuyan from '@/api/liuyan'
import article from '@/api/article'
// 轮播图
export const reqCarousel = () => {
    return mockRequest({
        url: '/carousel',
        method: 'GET',
    })
}
// 卡片
export const reqAlbum = () => {
    return mockRequest({
        url: '/album',
        method: 'GET',
    })
}
// miss部分
export const reqMissing = () => {
    return mockRequest({
        url: '/missing',
        method: 'GET',
    })
}
// footer部分
export const reqFooter = () => {
    return mockRequest({
        url: '/footer',
        method: 'GET'
    })
}
// 获取用户信息
export const reqUserInfo = () => {
    return userInfo({
        url: '/userinfo',
        method: 'GET'
    })
}
// 获取某个用户信息
export const reqUserInfoById = (data) => {
    return userInfo({
        url: '/userinfo2',
        method: 'POST',
        data
    })
}
// 获取全部用户信息
export const reqAllUserInfo = () => {
    return userInfo({
        url: '/alluserinfo',
        method: 'GET',
    })
}
// 修改密码
export const reqUserPwd = (data) => {
    return userInfo({
        url: '/updatepwd',
        method: "POST",
        data
    })
}
// 头像
export const reqAvatar = (data) => {
    return userInfo({
        url: '/update/avatar',
        method: 'POST',
        data
    })
}
// 更新用户信息
export const reqUpdateUserInfo = (data) => {
    return userInfo({
        url: '/update',
        method: 'POST',
        data
    })
}
// 获取留言
export const reqLiuyan = () => {
    return liuyan({
        url: '/messages',
        method: 'GET'
    })
}
// 新增留言
export const reqAddLiuyan = (data) => {
    return liuyan({
        url: '/pubmessages',
        method: 'POST',
        data
    })
}
// 删除留言
export const reqDeleteLiuyan = (id) => {
    return liuyan({
        url: `/deleteliuyan/${id}`,
        method: 'GET',
    })
}
// 搜索用户
export const reqSearchInfo = (data) => {
    return userInfo({
        url: `/search`,
        method: 'POST',
        data
    })
}
// 删除用户
export const reqDeleteUser = (data) => {
    return userInfo({
        url: `/delete`,
        method: 'POST',
        data
    })
}
// 新增文章发布
export const reqArticlePub = (data) => {
    return article({
        url: '/public',
        method: 'POST',
        data
    })
}
// 获取所有文章
export const reqArticles = () => {
    return article({
        url: '/getarticles',
        method: 'GET',
    })
}
// 删除文章
export const reqDeleteArticle = (id) => {
    return article({
        url: `/deletearticle/${id}`,
        method: 'GET'
    })
}




