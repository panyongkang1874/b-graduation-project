import Mock from 'mockjs'
import carousel from './carousel.json'
import album from './album.json'
import missing from '@/mock/missing'
Mock.mock('/mock/album', { code: 200, data: album })
Mock.mock('/mock/carousel', { code: 200, data: carousel })
Mock.mock('/mock/missing', { code: 200, data: missing })