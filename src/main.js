import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/mock'
import lazyPlugin from 'vue3-lazy'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ELIcons from '@element-plus/icons-vue'
import axios from 'axios'
const app = createApp(App)
for (let iconName in ELIcons) {
    app.component(iconName, ELIcons[iconName])
}
app.config.globalProperties.$axios = axios
app.provide('$axios', axios)
app.use(store).use(router).use(ElementPlus).use(lazyPlugin, {
    loading: require('@/assets/images/图片懒加载.jpg'), // 图片加载时默认图片
}).mount('#app')
