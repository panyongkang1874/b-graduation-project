const express = require('express')
// 创建路由对象 
const router = express.Router()
// 导入用户路由处理函数模块 
const userHandler = require('../router_handler/user')
// 导入数据验证中间件
const expressJoi = require('@escook/express-joi')
// 导入需要验证的规则对象
const { regLoginSchema } = require('../schema/user')
// 注册新用户 
router.post('/register', expressJoi(regLoginSchema), userHandler.register)
// 登录 
router.post('/login', userHandler.login)
// 将路由对象共享出去
module.exports = router