// 导入 express
const express = require('express')
// 创建路由对象
const router = express.Router()
// 导入文章分类的路由处理函数模块
const liuyan_handler = require('../router_handler/liuyan')
// 导入验证数据的中间件
const expressJoi = require('@escook/express-joi')
// 导入留言的验证模块 
const { delete_liuyan_schema } = require('../schema/liuyan')
// 获取留言的列表数据
router.get('/messages', liuyan_handler.getliuyan)
// 新增留言
router.post('/pubmessages', liuyan_handler.addliuyan)
// 删除留言
router.get('/deleteliuyan/:id', expressJoi(delete_liuyan_schema), liuyan_handler.deleteLiuyanById)
// 向外共享路由对象 
module.exports = router