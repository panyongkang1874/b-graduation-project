const express = require('express')
const userinfoHandler = require('../router_handler/userinfo')
// 创建路由对象 
const router = express.Router()
// 导入验证数据合法性的中间件
const expressJoi = require('@escook/express-joi')
// 导入需要的验证规则对象 
const { update_userinfo_schema, update_password_schema, update_avatar_schema, searchSchema, deleteSchema } = require('../schema/user')
// 获取用户的基本信息
router.get('/userinfo', userinfoHandler.getUserInfo)
// 获取所有用户信息
router.get('/alluserinfo', userinfoHandler.getAllUserInfo)
// 获取某个用户的基本信息
router.post('/userinfo2', userinfoHandler.getUserInfo2)
// 更新用户的基本信息 
router.post('/update', expressJoi(update_userinfo_schema), userinfoHandler.updateUserInfo)
// 重置密码的路由 
router.post('/updatepwd', expressJoi(update_password_schema), userinfoHandler.updatePassword)
// 更新用户头像的路由 
router.post('/update/avatar', expressJoi(update_avatar_schema), userinfoHandler.updateAvatar)
// 搜索用户
router.post('/search', expressJoi(searchSchema), userinfoHandler.searchInfo)
// 删除某个用户
router.post('/delete', expressJoi(deleteSchema), userinfoHandler.deleteUser)
// 将路由对象共享出去
module.exports = router