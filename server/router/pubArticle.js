// 导入 express
const express = require('express')
// 创建路由对象 
const router = express.Router()
// 导入文章的路由处理函数模块
const article_handler = require('../router_handler/pubArticle')
// 导入验证数据的中间件
const expressJoi = require('@escook/express-joi')
// 导入文章的验证模块 
const { pub_article_schema, delete_article_schema } = require('../schema/pubArticle')
// 发布新文章 
router.post('/public', expressJoi(pub_article_schema), article_handler.pubArticle)
// 获取所有文章
router.get('/getarticles', article_handler.getArticles)
// 删除文章
router.get('/deletearticle/:id', expressJoi(delete_article_schema), article_handler.deleteArticles)
// 向外共享路由对象
module.exports = router