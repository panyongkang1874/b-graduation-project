const express = require('express')
const cors = require('cors')
const app = express()
const bodyParser = require('body-parser');
const userRouter = require('./router/user')
const joi = require('joi')
const config = require('./config')
const userinfoRouter = require('./router/userinfo')
// 导入并使用文章路由模块
const publicArticleRouter = require('./router/pubArticle')
// 为文章分类的路由挂载统一的访问前缀 /my/article 

// 解析 application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// 解析 application/json
app.use(bodyParser.json());
app.use(cors())
//设置跨域请求
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});

// 响应数据的中间件
app.use(function (req, res, next) {
    // status = 0 为成功； status = 1 为失败； 默认将 status 的值设置为 1，方便处理失败的情况
    res.cc = function (err, status = 1) {
        res.send({
            // 状态 status, 
            // 状态描述，判断 err 是 错误对象 还是 字符串
            message: err instanceof Error ? err.message : err,
        })
    }
    next()
})

// 解析 token 的中间件 
const expressJWT = require('express-jwt')
// 使用 .unless({ path: [/^\/api\//] }) 指定哪些接口不需要进行 Token 的身份认证 
app.use(expressJWT({ secret: config.jwtSecretKey, algorithms: ['HS256'] }).unless({ path: [/^\/api\//] }))


app.use('/api', userRouter)
// 注意：以 /self 开头的接口，都是有权限的接口，需要进行 Token 身份认证 
app.use('/self', userinfoRouter)
// 为文章的路由挂载统一的访问前缀 /my/article
app.use('/self/article', publicArticleRouter)
// 导入并使用文章分类路由模块 
const liuyanRouter = require('./router/liuyan')
// 为留言路由挂载统一的访问前缀 /self/liuyan
app.use('/self/liuyan', liuyanRouter)
// 托管静态资源文件
app.use('/uploads', express.static('./uploads'))
// 错误中间件
app.use(function (err, req, res, next) {
    // 数据验证失败 
    if (err instanceof joi.ValidationError) return res.cc(err)
    // 捕获身份认证失败的错误 
    if (err.name === 'UnauthorizedError') return res.cc('身份认证失败！')
    // 未知错误 
    res.cc(err)
})

// 调用 app.listen 方法，指定端口号并启动web服务器
app.listen(3007, function () {
    console.log('api server running at http://127.0.0.1:3007')
})