const db = require('../db/index')
// 发布新文章的处理函数
exports.pubArticle = (req, res) => {
    const sql = `insert into ev_articles set ?`
    // 执行 SQL 语句 
    db.query(sql, req.body, (err, results) => {
        // 执行 SQL 语句失败 
        if (err) return res.cc(err)
        // 执行 SQL 语句成功，但是影响行数不等于 1 
        if (results.affectedRows !== 1)
            return res.cc('发布文章失败！')
        // 发布文章成功 
        res.cc('发布文章成功', 0)
    })
}
exports.getArticles = (req, res) => {
    const sql = 'select * from ev_articles where is_delete=0 order by id desc'
    db.query(sql, (err, results) => {
        if (err) return res.cc(err)
        res.send({
            status: 0,
            message: '获取文章成功！',
            data: results,
        })
    })
}
exports.deleteArticles = (req, res) => {
    const sql = 'update ev_articles set is_delete=1 where id=?'
    db.query(sql, req.params.id, (err, results) => {
        // 执行 SQL 语句失败
        if (err) return res.cc(err)
        // SQL 语句执行成功，但是影响行数不等于 1 
        if (results.affectedRows !== 1) return res.cc('删除留言失败！')
        // 删除文章分类成功
        res.send({
            status: 0,
            message: '文章删除成功!'
        })
    })
}