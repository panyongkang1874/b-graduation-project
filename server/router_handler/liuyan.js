// 导入数据库操作模块
const db = require('../db/index')

// 获取
exports.getliuyan = (req, res) => {
    // is_delete 为 0 表示没有被 标记为删除 的数据 
    const sql = 'select * from liuyan where is_delete=0 order by id desc'
    db.query(sql, (err, results) => {
        // 1. 执行 SQL 语句失败 
        if (err) return res.cc(err)
        // 2. 执行 SQL 语句成功
        res.send({
            status: 0,
            message: '获取留言成功！',
            data: results,
        })
    })

}
// 添加
exports.addliuyan = (req, res) => {
    // TODO：新增文章分类 
    const sql = `insert into liuyan set ?`
    db.query(sql, req.body, (err, results) => {
        // SQL 语句执行失败 
        if (err) return res.cc(err)
        // SQL 语句执行成功，但是影响行数不等于 1 
        if (results.affectedRows !== 1) return res.cc('新增留言失败！')
        // 新增文章分类成功 
        res.cc('新增留言成功！', 0)
    })
}
// 删除
exports.deleteLiuyanById = (req, res) => {
    const sql = `update liuyan set is_delete=1 where id=?`
    db.query(sql, req.params.id, (err, results) => {
        // 执行 SQL 语句失败
        if (err) return res.cc(err)
        // SQL 语句执行成功，但是影响行数不等于 1 
        if (results.affectedRows !== 1) return res.cc('删除留言失败！')
        // 删除文章分类成功
        res.send({
            status: 0,
            message: '删除成功!'
        })
    })
}