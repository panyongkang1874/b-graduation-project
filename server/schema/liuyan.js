// 导入定义验证规则的模块
const joi = require('joi')
// 定义 分类Id 的校验规则 
const id = joi.number().integer().min(1).required()
// 校验规则对象 - 删除分类
exports.delete_liuyan_schema = {
    params: {
        id,
    },
}
