// 导入定义验证规则包
const joi = require('joi')

// 定义用户名和密码的验证规则
// const username = joi.string().alphanum().min(1).max(9).required()
const username = joi.string().pattern(new RegExp('^[a-zA-Z][a-zA-Z0-9_]{1,6}$')).required()
const password = joi.string().pattern(new RegExp('^.{6,12}$')).required()
const email = joi.string().pattern(new RegExp('[0-9]{5,17}@(qq|QQ).com')).required()
const id = joi.number().integer().min(1).required()
const age = joi.number()
const sex = joi.string()
const phone = joi.number()
const study = joi.string()
const gitee = joi.string()
const city = joi.string()
const say = joi.string()
const keyWord = joi.string().required()
//dataUri()指的是如下格式的字符串数据：
//data:image/png;base64,VE9PTUFOWVNFQ1JFVFM=
const avatar = joi.string().dataUri().required()
// const nickname = joi.string().required()

// 定义登录注册表单规则对象
exports.regLoginSchema = {
    body: {
        username,
        password,
        email,

    }
}
// 定义更新用户信息的验证
exports.update_userinfo_schema = {
    body: {
        id,
        // nickname,
        username,
        email,
        age,
        sex,
        phone,
        study,
        gitee,
        city,
        say
    }
}
// 验证规则对象 - 重置密码
exports.update_password_schema = {
    body: {
        // 使用 password 这个规则，验证 req.body.oldPwd 的值 
        oldPwd: password,
        // 使用 joi.not(joi.ref('oldPwd')).concat(password) 规则，验证 req.body.newPwd 的值 // 解读： // 1. joi.ref('oldPwd') 表示 newPwd 的值必须和 oldPwd 的值保持一致 // 2. joi.not(joi.ref('oldPwd')) 表示 newPwd 的值不能等于 oldPwd 的值 // 3. .concat() 用于合并 joi.not(joi.ref('oldPwd')) 和 password 这两条验证规则 
        newPwd: joi.not(joi.ref('oldPwd')).concat(password),
    },
}
// 验证规则对象 - 更新头像
exports.update_avatar_schema = {
    body: {
        avatar,
    }
}
exports.searchSchema = {
    body: {
        keyWord
    }
}
exports.deleteSchema = {
    body: {
        id
    }
}




