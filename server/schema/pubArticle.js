// 导入定义验证规则的模块
const joi = require('joi')
// 定义 标题、分类Id、内容、发布状态 的验证规则
const title = joi.string().required()
const content = joi.string().required().allow('')
const pub_data = joi.required()
const cover_img = joi.required()
const id = joi.number().integer().min(1).required()
// 验证规则对象 - 发布文章
exports.pub_article_schema = {
    body: {
        title,
        content,
        pub_data,
        cover_img
    },
}
exports.delete_article_schema = {
    params: {
        id,
    }
}