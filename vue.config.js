const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 百度地图
  configureWebpack: {
    externals: {
      "BMap": "BMap"
    }
  },
  lintOnSave: false,
  devServer: {
    host: '0.0.0.0',  // 本地主机
    proxy: {
      '/api': {   //  拦截以 /api 开头的接口
        target: 'http://127.0.0.1:3007',//设置你调用的接口域名和端口号 别忘了加http
        changeOrigin: true,    //这里true表示实现跨域
      },
      '/self': {   //  拦截以 /api 开头的接口
        target: 'http://127.0.0.1:3007',//设置你调用的接口域名和端口号 别忘了加http
        changeOrigin: true,    //这里true表示实现跨域
      },
    }
  }

})
