module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/vue3-essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false

  },
  rules: {
    // allow async-await

    'generator-star-spacing': 'off',

    // allow debugger during development

    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    "semi": 0, // 去掉结尾的分号

    "singleQuote": 0, // 单引号替代双引号

    "trailingComma": "none",// 末尾禁止添加逗号

    "indent": 0, //首行缩进2/4字符问题

    "space-before-function-paren": 0  //函数前空格

  }
  // globals: {
  //   BMap: true
  // }
}
